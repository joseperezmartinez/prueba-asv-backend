# Prueba ASV Backend

Backend Spring Boot para la prueba fullstack para ASV. El backend es autocontenido. Para pruebas posee una base de datos mínima basada en H2. Se despliega al arrancar el servicio.

## Despliegue

* Requisitos son mvn y jdk 1.8.0+.

- Descsrgar el proyecto
- Descargar dependencias.
  - ``mvn install``
- Se puede ejecutar en local 
  - ``java -jar ${DEST_FOLDER}/pruebaasvbackend-0.0.1-SNAPSHOT.jar``
- Se puede instalar en una máquina. Ejecutar con el comando anterior.

