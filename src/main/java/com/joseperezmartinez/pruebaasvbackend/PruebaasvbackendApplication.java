package com.joseperezmartinez.pruebaasvbackend;

import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Pruebaasvbackend application.
 */
@SpringBootApplication
@EnableJSONDoc
public class PruebaasvbackendApplication {

    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        SpringApplication.run(PruebaasvbackendApplication.class, args);
    }
}
