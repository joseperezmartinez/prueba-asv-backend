package com.joseperezmartinez.pruebaasvbackend.configuration;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * The type Mapper configuration.
 */
@Configuration
public class MapperConfiguration {

    /**
     * Dozer bean dozer bean mapper.
     *
     * @return the dozer bean mapper
     */
    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper dozerBean() {

        final List<String> mappingFiles = Arrays.asList("dozer-global-configuration.xml");
        final DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        return dozerBean;
    }

}
