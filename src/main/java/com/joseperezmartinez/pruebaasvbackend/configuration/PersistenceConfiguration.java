package com.joseperezmartinez.pruebaasvbackend.configuration;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.SessionFactoryUtils;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * The type Persistence configuration.
 */
@Configuration
@EnableJpaRepositories(value = "com.joseperezmartinez.pruebaasvbackend.persistence.repository")
@EnableTransactionManagement
public class PersistenceConfiguration {

    @Value("${spring.datasource.url}")
    private String jdbcUrl;

    @Value("${spring.datasource.driver-class-name}")
    private String driver;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String dialect;

    /**
     * Entity manager factory.
     *
     * @return the local container entity manager factory bean
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

        final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setJpaVendorAdapter(vendorAdapter());
        bean.setDataSource(dataSource());
        bean.setPersistenceUnitName("asv-persistence");
        bean.setPackagesToScan("com.joseperezmartinez.pruebaasvbackend.persistence.entity");

        final Properties props = new Properties();
        props.setProperty("hibernate.show_sql", "true");
        props.setProperty("hibernate.format_sql", "true");
        props.setProperty("hibernate.use_sql_comments", "true");
        bean.setJpaProperties(props);
        return bean;
    }

    /**
     * Vendor adapter.
     *
     * @return the jpa vendor adapter
     */
    @Bean
    public JpaVendorAdapter vendorAdapter() {

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabasePlatform(dialect);
        vendorAdapter.setShowSql(false);
        return vendorAdapter;
    }

    /**
     * Creates datasource.
     *
     * @return the data source
     */
    @Bean
    public DataSource dataSource() {

        final Properties props = new Properties();
        props.setProperty("driverClassName", driver);
        props.setProperty("username", username);
        props.setProperty("password", password);
        props.setProperty("url", jdbcUrl);

        DataSource dataSource = null;
        try {
            dataSource = BasicDataSourceFactory.createDataSource(props);
        } catch (final Exception e) {
            throw new ApplicationContextException("Error creating datasource", e);
        }
        return dataSource;
    }

    /**
     * Transaction manager.
     *
     * @return the jpa transaction manager
     */
    @Bean
    @Primary
    public JpaTransactionManager transactionManager() {

        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    /**
     * Builds the data access exception translator
     *
     * @return the data access exception translator
     */
    @Bean
    public PersistenceExceptionTranslator persistenceExceptionTranslator() {
        return new CustomPersistenceExceptionTranslator();
    }

    /**
     * Custom data access exceptions translator
     *
     * @author hernan.perez
     */
    private static final class CustomPersistenceExceptionTranslator implements PersistenceExceptionTranslator {

        @Override
        public DataAccessException translateExceptionIfPossible(final RuntimeException ex) {
            // Converts Hibernate Exceptions in to DataAccessException
            if (ex.getCause() instanceof HibernateException) {
                return SessionFactoryUtils.convertHibernateAccessException((HibernateException) ex.getCause());
            } else {
                return new DataAccessException("A not controlled data access failure occurs.", ex) {
                    private static final long serialVersionUID = 1L;
                };
            }
        }
    }

}
