package com.joseperezmartinez.pruebaasvbackend.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.validation.constraints.Size;

/**
 * The type PokemonExceptionHandler resource.
 */
@ApiObject(name = "PokemonResource", description = "PokemonExceptionHandler resource information")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PokemonResource {

    @ApiObjectField(description = "The identifier")
    private Long id;

    @ApiObjectField(description = "The name")
    @NotEmpty(message = ValidationConstants.STRING_NOT_EMPTY)
    @Size(max = ValidationConstants.NAME_STRING_LENGTH, message = ValidationConstants.ERRORTYPE_ELEMENTTYPE_SIZE_MAX)
    private String name;

    @ApiObjectField(description = "The description")
    @Size(max = ValidationConstants.LARGE_STRING_LENGTH, message = ValidationConstants.ERRORTYPE_ELEMENTTYPE_SIZE_MAX)
    private String description;

    @ApiObjectField(description = "PokemonExceptionHandler type 1")
    @Size(max = ValidationConstants.ENUM_STRING_LENGTH, message = ValidationConstants.ERRORTYPE_ELEMENTTYPE_SIZE_MAX)
    private String firstType;

    @ApiObjectField(description = "PokemonExceptionHandler type 2")
    @Size(max = ValidationConstants.ENUM_STRING_LENGTH, message = ValidationConstants.ERRORTYPE_ELEMENTTYPE_SIZE_MAX)
    private String secondType;

    @ApiObjectField(description = "Favorite flag")
    @JsonProperty("favorite")
    private final Boolean isFavorite = false;

    @ApiObjectField(description = "Link to the next pokemon to evolve")
    private PokemonResource evolvesTo;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets first type.
     *
     * @return the first type
     */
    public String getFirstType() {
        return firstType;
    }

    /**
     * Sets first type.
     *
     * @param firstType the first type
     */
    public void setFirstType(final String firstType) {
        this.firstType = firstType;
    }

    /**
     * Gets second type.
     *
     * @return the second type
     */
    public String getSecondType() {
        return secondType;
    }

    /**
     * Sets second type.
     *
     * @param secondType the second type
     */
    public void setSecondType(final String secondType) {
        this.secondType = secondType;
    }

    /**
     * Gets favorite.
     *
     * @return the favorite
     */
    public Boolean getFavorite() {
        return isFavorite;
    }

    /**
     * Gets evolves to.
     *
     * @return the evolves to
     */
    public PokemonResource getEvolvesTo() {
        return evolvesTo;
    }

    /**
     * Sets evolves to.
     *
     * @param evolvesTo the evolves to
     */
    public void setEvolvesTo(final PokemonResource evolvesTo) {
        this.evolvesTo = evolvesTo;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof PokemonResource)) {
            return false;
        }

        final PokemonResource that = (PokemonResource) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(name, that.name)
                .append(description, that.description)
                .append(firstType, that.firstType)
                .append(secondType, that.secondType)
                .append(isFavorite, that.isFavorite)
                .append(evolvesTo, that.evolvesTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(description)
                .append(firstType)
                .append(secondType)
                .append(isFavorite)
                .append(evolvesTo)
                .toHashCode();
    }
}
