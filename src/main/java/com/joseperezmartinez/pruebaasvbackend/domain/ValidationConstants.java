package com.joseperezmartinez.pruebaasvbackend.domain;

public class ValidationConstants {
    public final static int ENUM_STRING_LENGTH = 15;
    public final static int NAME_STRING_LENGTH = 24;
    public final static int LARGE_STRING_LENGTH = 30;

    public final static String STRING_NOT_EMPTY = "The string can't be empty.";
    public final static String ERRORTYPE_ELEMENTTYPE_SIZE_MAX = "The size of this field is to high,";
}
