package com.joseperezmartinez.pruebaasvbackend.exception.handler;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.Map;

@ControllerAdvice
@RestController
public class PokemonExceptionHandler extends ResponseEntityExceptionHandler {

    private final static String NOT_FOUND_ERROR = "This entity don't exists.";
    private final static String DATA_INTEGRATION_VIOLATION_ERROR = "You try to persist data with a data integrity violation";

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> handleException(final Exception exception) {
        return createResponse(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> handleException(final EntityNotFoundException exception) {
        return createResponse(NOT_FOUND_ERROR, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> handleException(final DataIntegrityViolationException exception) {
        if (exception.getCause() instanceof ConstraintViolationException) {
            return createResponse(DATA_INTEGRATION_VIOLATION_ERROR, HttpStatus.BAD_REQUEST);
        }
        throw exception;
    }

    protected ResponseEntity<Map<String, Object>> createResponse(final String message, final HttpStatus statusCode) {
        final HttpStatus httpStatusCode = (statusCode != null) ? statusCode : HttpStatus.INTERNAL_SERVER_ERROR;
        final HttpHeaders headers = new HttpHeaders();

        headers.put("X-Powered-By", Arrays.asList("Iptool"));
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity(message, headers, httpStatusCode);
    }

}
