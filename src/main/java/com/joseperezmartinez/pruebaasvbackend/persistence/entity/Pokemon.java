package com.joseperezmartinez.pruebaasvbackend.persistence.entity;

import com.joseperezmartinez.pruebaasvbackend.domain.ValidationConstants;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * The type PokemonExceptionHandler.
 */
@Entity
@Table(name = "POKEMON")
@NamedQuery(name = "Pokemon.findAll", query="SELECT p FROM Pokemon p ORDER BY p.id")
public class Pokemon {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "POKEMON_ID")
    private Long id;

    @Column(name = "NAME", columnDefinition = "VARCHAR", nullable = false, length = ValidationConstants.NAME_STRING_LENGTH)
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "VARCHAR", length = ValidationConstants.LARGE_STRING_LENGTH)
    private String description;

    @Column(name = "FIRST_TYPE", columnDefinition = "VARCHAR", length = ValidationConstants.ENUM_STRING_LENGTH)
    @Enumerated(EnumType.STRING)
    private PokemonType firstType;

    @Column(name = "SECOND_TYPE", columnDefinition = "VARCHAR", length = ValidationConstants.ENUM_STRING_LENGTH)
    @Enumerated(EnumType.STRING)
    private PokemonType secondType;

    @Column(name = "IS_FAVORITE", columnDefinition = "INT", nullable = false)
    private Boolean isFavorite = false;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVOLVES_IN")
    private Pokemon evolvesTo;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets favorite.
     *
     * @return the favorite
     */
    public Boolean getFavorite() {
        return isFavorite;
    }

    /**
     * Sets favorite.
     *
     * @param favorite the favorite
     */
    public void setFavorite(final Boolean favorite) {
        isFavorite = favorite;
    }

    /**
     * Gets first type.
     *
     * @return the first type
     */
    public PokemonType getFirstType() {
        return firstType;
    }

    /**
     * Sets first type.
     *
     * @param firstType the first type
     */
    public void setFirstType(final PokemonType firstType) {
        this.firstType = firstType;
    }

    /**
     * Gets second type.
     *
     * @return the second type
     */
    public PokemonType getSecondType() {
        return secondType;
    }

    /**
     * Sets second type.
     *
     * @param secondType the second type
     */
    public void setSecondType(final PokemonType secondType) {
        this.secondType = secondType;
    }

    /**
     * Gets evolves to.
     *
     * @return the evolves to
     */
    public Pokemon getEvolvesTo() {
        return evolvesTo;
    }

    /**
     * Sets evolves to.
     *
     * @param evolvesIn the evolves to
     */
    public void setEvolvesTo(final Pokemon evolvesIn) {
        evolvesTo = evolvesIn;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Pokemon)) {
            return false;
        }

        final Pokemon pokemon = (Pokemon) o;

        return new EqualsBuilder()
                .append(id, pokemon.id)
                .append(name, pokemon.name)
                .append(description, pokemon.description)
                .append(isFavorite, pokemon.isFavorite)
                .append(firstType, pokemon.firstType)
                .append(secondType, pokemon.secondType)
                .append(evolvesTo, pokemon.evolvesTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(description)
                .append(isFavorite)
                .append(firstType)
                .append(secondType)
                .append(evolvesTo)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("name", name)
                .append("firstType", firstType)
                .append("secondType", secondType)
                .append("isFavorite", isFavorite)
                .toString();
    }
}
