package com.joseperezmartinez.pruebaasvbackend.persistence.entity;

import java.util.Arrays;

public enum PokemonType {
    BICHO,
    DRAGON,
    HADA,
    FUEGO,
    FANTASMA,
    TIERRA,
    NORMAL,
    PSIQUICO,
    ACERO,
    SINIESTRO,
    ELECTRICO,
    LUCHA,
    VOLADOR,
    PLANTA,
    HIELO,
    VENENO,
    ROCA,
    AGUA;
}
