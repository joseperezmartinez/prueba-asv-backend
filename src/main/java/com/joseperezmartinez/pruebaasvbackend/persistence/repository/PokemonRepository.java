package com.joseperezmartinez.pruebaasvbackend.persistence.repository;

import com.joseperezmartinez.pruebaasvbackend.persistence.entity.Pokemon;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.PokemonType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The interface PokemonExceptionHandler repository.
 */
@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Long> {

    @Override
    @Query
    List<Pokemon> findAll();

    @Override
    Optional<Pokemon> findById(Long id);

    @Query("SELECT p FROM Pokemon p ORDER BY p.id")
    List<Pokemon> findAllPokemonsByPage(final Pageable pageable);

    /**
     * Find by name list.
     *
     * @param name the name
     * @return the list
     */
    List<Pokemon> findByName(final String name);

    /**
     * Find by first type list.
     *
     * @param firstType the first type
     * @return the list
     */
    List<Pokemon> findByFirstType(final PokemonType firstType);

    /**
     * Find by second type list.
     *
     * @param secondType the second type
     * @return the list
     */
    List<Pokemon> findBySecondType(final PokemonType secondType);

    /**
     * Find by is favorite list.
     *
     * @param favorite the favorite
     * @return the list
     */
    List<Pokemon> findByIsFavorite(final Boolean favorite);
}
