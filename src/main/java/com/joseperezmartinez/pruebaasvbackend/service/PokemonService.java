package com.joseperezmartinez.pruebaasvbackend.service;

import com.joseperezmartinez.pruebaasvbackend.domain.PokemonResource;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * The interface PokemonExceptionHandler service.
 */
public interface PokemonService {

    /**
     * Get the total number of pokemons.
     *
     * @return
     */
    long countPokemons();

    /**
     * Gets listing.
     *
     * @param pageRequest the page request
     * @return the listing
     */
    List<PokemonResource> getListing(Pageable pageRequest);

    /**
     * Gets pokemon.
     *
     * @param id the id
     * @return the pokemon
     */
    PokemonResource getPokemon(Long id);

    /**
     * Create pokemon resource.
     *
     * @param pokemon the pokemon
     * @return the pokemon resource
     */
    PokemonResource create(PokemonResource pokemon);

    /**
     * Update.
     *
     * @param pokemon the pokemon
     */
    void update(PokemonResource pokemon);

    /**
     * Remove.
     *
     * @param id the id
     */
    void remove(Long id);

}
