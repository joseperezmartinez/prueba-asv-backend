package com.joseperezmartinez.pruebaasvbackend.service.impl;

import com.joseperezmartinez.pruebaasvbackend.domain.PokemonResource;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.Pokemon;
import com.joseperezmartinez.pruebaasvbackend.persistence.repository.PokemonRepository;
import com.joseperezmartinez.pruebaasvbackend.service.PokemonService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type PokemonExceptionHandler service.
 */
@Service
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Autowired
    private Mapper dozerMapper;

    @Override
    public long countPokemons() {
        return pokemonRepository.count();
    }

    @Override
    public List<PokemonResource> getListing(final Pageable pageRequest) {

        return dozerMapper.map(pokemonRepository.findAllPokemonsByPage(pageRequest), List.class);
    }

    @Override
    public PokemonResource getPokemon(final Long id) {

        try {
            final Pokemon pokemon = pokemonRepository.getOne(id);
            return dozerMapper.map(pokemon, PokemonResource.class);
        } catch (final Exception e) {
            throw new EntityNotFoundException("Not exists any pokemon with ID " + id);
        }
    }

    @Transactional
    @Override
    public PokemonResource create(final PokemonResource pokemon) {

        final Pokemon pokemonData = dozerMapper.map(pokemon, Pokemon.class);
        return dozerMapper.map(pokemonRepository.saveAndFlush(pokemonData), PokemonResource.class);
    }

    @Transactional
    @Override
    public void update(final PokemonResource pokemon) {

        try {
            final Pokemon pokemonData = dozerMapper.map(pokemon, Pokemon.class);
            pokemonRepository.saveAndFlush(pokemonData);
        } catch (final Exception e) {
            throw new EntityNotFoundException("Not exists any pokemon with ID " + pokemon.getId());
        }
    }

    @Transactional
    @Override
    public void remove(final Long id) {

        pokemonRepository.deleteById(id);
        pokemonRepository.flush();
    }
}
