package com.joseperezmartinez.pruebaasvbackend.ws.controller;

import com.joseperezmartinez.pruebaasvbackend.persistence.entity.Pokemon;
import com.joseperezmartinez.pruebaasvbackend.persistence.repository.PokemonRepository;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Dummy controller.
 */
@Api(name = "Dummy controller", description = "Basic REST endpoints", group = "Main")
@RestController
public class DummyController {

    @Autowired
    private PokemonRepository pokemonRepository;

    /**
     * Foo string.
     *
     * @return the string
     */
    @ApiMethod(description = "Return the hello-world string.")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponseObject
    @RequestMapping(path = "/helloworld", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String foo() {
        return "Hello World!";
    }

    /**
     * List list.
     *
     * @return the list
     */
    @ApiMethod(description = "Return list of pokemons.")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponseObject
    @RequestMapping(path = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Pokemon> list() {
        return pokemonRepository.findAll();
    }

}
