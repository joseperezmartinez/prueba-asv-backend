package com.joseperezmartinez.pruebaasvbackend.ws.controller;

import com.joseperezmartinez.pruebaasvbackend.domain.PokemonResource;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.PokemonType;
import com.joseperezmartinez.pruebaasvbackend.service.PokemonService;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

@Api(name = "PokemonExceptionHandler services", description = "Methods for managing pokemons", group = "Main")
@RestController
@RequestMapping("/pokemon")
@CrossOrigin
public class PokemonController {

    @Autowired
    private PokemonService pokemonService;

    @ApiMethod(description = "Return the list of pokemon types")
    @ApiResponseObject
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(path = "types", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String[] getTypes() {

        return getNames(PokemonType.class);
    }

    private static String[] getNames(final Class<? extends Enum<?>> e) {

        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }

    @ApiMethod(description = "Return a list of pokemons")
    @ApiResponseObject
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<PokemonResource> getListing(@RequestParam final int page, @RequestParam final int limit) {

        final Pageable pageRequest = PageRequest.of(page, limit);
        final List<PokemonResource> list = pokemonService.getListing(pageRequest);
        final long totalPokemons = pokemonService.countPokemons();

        return new PageImpl<>(list, pageRequest, totalPokemons);
    }

    @ApiMethod(description = "Get a pokemon")
    @ApiResponseObject
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(path = "{pokemonId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PokemonResource getItem(@ApiPathParam(name = "pokemonId") @PathVariable @NotNull final long pokemonId) {

        return pokemonService.getPokemon(pokemonId);
    }

    @ApiMethod(description = "Create a new pokemon")
    @ApiResponseObject
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PokemonResource create(@RequestBody @Valid final PokemonResource pokemon) {

        return pokemonService.create(pokemon);
    }

    @ApiMethod(description = "Update a pokemon")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(path = "/{pokemonId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(@ApiPathParam(name = "pokemonId") @PathVariable @NotNull final long pokemonId, @RequestBody @Valid final PokemonResource pokemon) {

        pokemon.setId(pokemonId);
        pokemonService.update(pokemon);
    }

    @ApiMethod(description = "Update a pokemon favorite status")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(path = "/{pokemonId}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateFavorite(@ApiPathParam(name = "pokemonId") @PathVariable @NotNull final long pokemonId, @RequestBody @Valid final PokemonResource pokemon) {

        pokemon.setId(pokemonId);
        pokemonService.update(pokemon);
    }

    @ApiMethod(description = "Delete a pokemon")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(path = "/{pokemonId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@ApiPathParam(name = "pokemonId") @PathVariable @NotNull final long pokemonId) {

        pokemonService.remove(pokemonId);
    }

}
