package com.joseperezmartinez.pruebaasvbackend.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.api.PodamUtils;
import uk.co.jemos.podam.typeManufacturers.IntTypeManufacturerImpl;
import uk.co.jemos.podam.typeManufacturers.TypeManufacturer;

import java.sql.Timestamp;

/**
 * The type Persistence it conf.
 */
@Configuration
@EnableConfigurationProperties
@PropertySource(value = "classpath:/test-db.properties", ignoreResourceNotFound = true)
@ComponentScan(basePackages = {"com.joseperezmartinez.pruebaasvbackend"}, excludeFilters = {@ComponentScan.Filter(value = PersistenceConfiguration.class, type = FilterType.ASSIGNABLE_TYPE)})
@EnableJpaRepositories(value = "com.joseperezmartinez.pruebaasvbackend.persistence.repository")
public class PersistenceITConf {

    /**
     * Gets podam factory.
     *
     * @return the podam factory
     */
    @Bean
    public PodamFactory getPodamFactory() {
        final PodamFactory factory = new PodamFactoryImpl();

        final TypeManufacturer<Integer> intManufacturer = new IntTypeManufacturerImpl() {

            @Override
            public Integer getInteger(final AttributeMetadata attributeMetadata) {

                if (attributeMetadata.getPojoClass() == Timestamp.class) {
                    return PodamUtils.getIntegerInRange(0, 99999999);
                } else {
                    return super.getInteger(attributeMetadata);
                }
            }
        };

        factory.getStrategy().setDefaultNumberOfCollectionElements(0);
        factory.getStrategy().setMemoization(true);
        factory.getStrategy().addOrReplaceTypeManufacturer(int.class, intManufacturer);
        return factory;
    }
}
