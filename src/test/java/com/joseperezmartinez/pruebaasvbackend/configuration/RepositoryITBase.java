package com.joseperezmartinez.pruebaasvbackend.configuration;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Repository it base.
 */
@ContextConfiguration(classes = {RepositoryITBase.class})
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = RepositoryITBase.CLEAN_DB_XML)
@Transactional
public class RepositoryITBase {

    /**
     * The constant CLEAN_DB_XML.
     */
    public static final String CLEAN_DB_XML = "/persistence/cleanDatabase.xml";
}
