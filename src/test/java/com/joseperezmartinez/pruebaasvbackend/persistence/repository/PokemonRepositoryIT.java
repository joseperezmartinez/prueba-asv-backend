package com.joseperezmartinez.pruebaasvbackend.persistence.repository;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.joseperezmartinez.pruebaasvbackend.configuration.PersistenceITConf;
import com.joseperezmartinez.pruebaasvbackend.configuration.RepositoryITBase;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.Pokemon;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.PokemonType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.*;

/**
 * The type PokemonExceptionHandler repository it.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PersistenceITConf.class})
public class PokemonRepositoryIT extends RepositoryITBase {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Autowired
    private PodamFactory podamFactory;

    @Before
    public void setupDB() {

    }

    /**
     * When find all success.
     */
    @Test
    @DatabaseSetup("classpath:/startup.sql")
    public void whenFindAllSuccess() {

        // Arrange

        // Act
        final List<Pokemon> result = pokemonRepository.findAll();

        // Assert
        assertNotNull("Result is NULL", result);
        assertThat(result.size(), greaterThan(0));
    }

    /**
     * When find by page then success.
     */
    @Test
    @DatabaseSetup("classpath:/startup.sql")
    public void whenFindByPageThenSuccess() {
        // Arrange
        final Pageable pageRequest = new PageRequest(0, 4);

        // Act

        final List<Pokemon> result = pokemonRepository.findAllPokemonsByPage(pageRequest);

        // Assert
        assertNotNull("Result is NULL", result);
        assertEquals("Must be 4 elements in the content", 4, result.size());
    }

    /**
     * When create then return success.
     */
    @Test
    @DatabaseSetup("classpath:/startup.sql")
    public void whenCreateThenReturnSuccess() {

        // Arrange
        final long count_before_insert = pokemonRepository.findAll().size();
        final Pokemon pokemon = new Pokemon();
        pokemon.setName("test");
        pokemon.setFirstType(PokemonType.AGUA);

        // Act
        final Pokemon createdEntity = pokemonRepository.save(pokemon);
        final long count_after_insert = pokemonRepository.findAll().size();

        // Assert
        assertNotNull("createdEntity returned is null", createdEntity);
        assertEquals("Element not inserted", 1, count_after_insert - count_before_insert);
    }

    /**
     * When update then return success.
     */
    @Test
    @DatabaseSetup("classpath:/startup.sql")
    public void whenUpdateThenReturnSuccess() {

        // Arrange

        // Act
        final Pokemon searchEntity = pokemonRepository.findById(1L).get();
        searchEntity.setName("test");
        pokemonRepository.saveAndFlush(searchEntity);
        final Pokemon updateEntity = pokemonRepository.findById(1L).get();

        // Assert
        assertNotNull("createdEntity returned is null", searchEntity);
        assertEquals("PokemonExceptionHandler are different. Don't update data", searchEntity, updateEntity);
    }

    /**
     * When delete then return success.
     */
    @Test
    @DatabaseSetup("classpath:/startup.sql")
    public void whenDeleteThenReturnSuccess() {

        // Arrange
        final Long removeId = 1L;
        final long count_before_delete = pokemonRepository.findAll().size();

        // Act
        final Optional<Pokemon> entity = pokemonRepository.findById(removeId);
        pokemonRepository.delete(entity.get());
        final long count_after_delete = pokemonRepository.findAll().size();

        // Assert
        assertEquals("PokemonExceptionHandler not removed", 1, count_before_delete - count_after_delete);
    }

}
