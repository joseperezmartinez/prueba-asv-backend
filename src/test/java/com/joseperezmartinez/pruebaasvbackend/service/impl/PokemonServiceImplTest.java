package com.joseperezmartinez.pruebaasvbackend.service.impl;

import com.joseperezmartinez.pruebaasvbackend.domain.PokemonResource;
import com.joseperezmartinez.pruebaasvbackend.persistence.entity.Pokemon;
import com.joseperezmartinez.pruebaasvbackend.persistence.repository.PokemonRepository;
import org.dozer.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * The type Pokemon service impl test.
 */
@RunWith(MockitoJUnitRunner.class)
public class PokemonServiceImplTest {

    @Mock
    private PokemonRepository pokemonRepository;

    @Mock
    private Mapper dozerMapper;

    @Spy
    @InjectMocks
    private final PokemonServiceImpl pokemonService = new PokemonServiceImpl();

    /**
     * When request a page then success.
     */
    @Test
    public void whenRequestAPageThenSuccess() {

        // Arrsange
        final PageRequest pageRequest = PageRequest.of(1, 1);
        final List<Pokemon> page = Arrays.asList(new Pokemon());
        final List<PokemonResource> listResource = Arrays.asList(new PokemonResource());
        when(pokemonRepository.findAllPokemonsByPage(any(Pageable.class))).thenReturn(page);
        when(dozerMapper.map(any(), any())).thenReturn(listResource);

        // Act
        final List<PokemonResource> result = pokemonService.getListing(pageRequest);

        // Assert
        assertNotNull("Result is NULL", result);
        assertEquals("Must be 1 elements in the content", 1, result.size());
        verify(pokemonRepository, times(1)).findAllPokemonsByPage(any(Pageable.class));
    }

    /**
     * When request a non existing page then retur nothing.
     */
    @Test
    public void whenRequestANonExistingPageThenReturNothing() {

        // Arrsange
        final PageRequest pageRequest = PageRequest.of(10, 1);
        final List<Pokemon> page = Arrays.asList();
        final List<PokemonResource> listResource = Arrays.asList();
        when(pokemonRepository.findAllPokemonsByPage(any(Pageable.class))).thenReturn(page);
        when(dozerMapper.map(any(), any())).thenReturn(listResource);

        // Act
        final List<PokemonResource> result = pokemonService.getListing(pageRequest);

        // Assert
        assertNotNull("Result is NULL", result);
        assertEquals("Must be 0 elements in the content", 0, result.size());
        verify(pokemonRepository, times(1)).findAllPokemonsByPage(any(Pageable.class));
    }

    /**
     * When get a pokemon then success.
     */
    @Test
    public void whenGetAPokemonThenSuccess() {

        // Arrsange
        when(pokemonRepository.getOne(anyLong())).thenReturn(mock(Pokemon.class));
        when(dozerMapper.map(any(), any())).thenReturn(mock(PokemonResource.class));

        // Act
        final PokemonResource pokemon = pokemonService.getPokemon(1L);

        // Assert
        assertNotNull("Pokemon is NULL", pokemon);
        verify(pokemonRepository, times(1)).getOne(anyLong());
    }

    /**
     * When get a non existent pokemon then return nothing.
     */
    @Test
    public void whenGetANonExistentPokemonThenReturnNothing() {

        // Arrsange
        when(pokemonRepository.getOne(anyLong())).thenReturn(null);
        when(dozerMapper.map(any(), any())).thenReturn(null);

        // Act
        final PokemonResource pokemon = pokemonService.getPokemon(1L);

        // Assert
        assertNull("Pokemon is not NULL", pokemon);
        verify(pokemonRepository, times(1)).getOne(anyLong());
    }

    /**
     * When create a pokemon then success.
     */
    @Test
    public void whenCreateAPokemonThenSuccess() {

        // Arrsange
        final Pokemon pokemonData = mock(Pokemon.class);
        final PokemonResource input = mock(PokemonResource.class);
        final PokemonResource output = new PokemonResource();
        output.setName("test");
        output.setId(99L);

        when(dozerMapper.map(any(PokemonResource.class), any())).thenReturn(pokemonData);
        when(dozerMapper.map(any(Pokemon.class), any())).thenReturn(output);
        when(pokemonRepository.saveAndFlush(any(Pokemon.class))).then(returnsFirstArg());

        final PokemonResource expectedPokemon = new PokemonResource();
        expectedPokemon.setName("test");
        expectedPokemon.setId(99L);

        // Act
        final PokemonResource createdPokemon = pokemonService.create(input);

        // Assert
        assertNotNull("Result is NULL,", createdPokemon);
        assertThat(createdPokemon, is(expectedPokemon));
        verify(pokemonRepository, times(1)).saveAndFlush(any(Pokemon.class));
    }

    /**
     * When update a pokemon then success.
     */
    @Test
    public void whenUpdateAPokemonThenSuccess() {

        // Arrsange
        final Pokemon pokemonData = mock(Pokemon.class);
        final PokemonResource input = mock(PokemonResource.class);
        when(dozerMapper.map(any(PokemonResource.class), any())).thenReturn(pokemonData);
        when(pokemonRepository.saveAndFlush(any(Pokemon.class))).then(returnsFirstArg());

        // Act
        pokemonService.update(input);

        // Assert
        verify(pokemonRepository, times(1)).saveAndFlush(any(Pokemon.class));
    }

    /**
     * When remove pokemon then success.
     */
    @Test
    public void whenRemovePokemonThenSuccess() {

        // Arrsange
        final PokemonResource input = mock(PokemonResource.class);
        doNothing().when(pokemonRepository).deleteById(anyLong());

        // Act
        pokemonService.remove(1L);

        // Assert
        verify(pokemonRepository, times(1)).deleteById(anyLong());
        verify(pokemonRepository, times(1)).flush();
    }
}
